![Build Status](https://gitlab.com/el3ctre/pogo/badges/master/build.svg)

---

[Pages](https://el3ctre.gitlab.io/pogo)

---

## History

2023/09 : initial import of list by family

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

All HTML files are expected to be put in the `public/` directory.

